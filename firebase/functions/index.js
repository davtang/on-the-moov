const functions = require("firebase-functions");
const admin = require("firebase-admin");
admin.initializeApp();

/**
 *  Performs an asynchronous request to retrieve a document from Firestore
 * @param {String=} docRef
 * @return {any} data of the retrieved team member
 */
async function getDoc(docRef) {
  const docResponse = await docRef.get();
  console.log("MEMBER RETRIEVED:", docResponse);
  return docResponse.data();
}

// Listener method for sending push notification when new member joins team
exports.sendListenerPushNotificationTeamJoin =
    functions.firestore.document(
        "/disabled/{userId}").onUpdate( (snapshot, context) => {
      const beforeTeam = snapshot.before.data();
      const changedTeam = snapshot.after.data();
      if (changedTeam.team.length > beforeTeam.team.length) {
        const newMemberId = changedTeam.team[changedTeam.team.length - 1];
        const memberRef = admin.firestore().collection("helper")
            .doc(`${newMemberId}`);
        return getDoc(memberRef).then( (member) => {
          const FCMToken = changedTeam.fcm_token;
          console.log("FCM TOKEN:", FCMToken);
          const payload = {
            notification: {
              title: "OnTheMoov",
              body: `${member.name} has joined your team!`,
            },
          };
          return admin.messaging().sendToDevice(FCMToken, payload).then(
              (response) => {
                console.log("Successfully sent message:", response);
                return {success: true};
              }).catch((error) => {
            console.log("ERROR ENCOUNTERED:", error);
            return {error: error.code};
          });
        });
      }

      console.log("No message sent, no change detected.");
      return {success: true};
    });


// Listener method for sending push notification when help request sent
exports.sendListenerPushNotificationRequestSent =
    functions.firestore.document(
        "/helper/{userId}").onUpdate( (snapshot, context) => {
      const beforeHelper = snapshot.before.data();
      const afterHelper = snapshot.after.data();
      if (beforeHelper.active_request == null &&
        afterHelper.active_request != null) {
        const teamRef = admin.firestore().collection("disabled")
            .doc(`${afterHelper.my_team}`);
        return getDoc(teamRef).then( (team) => {
          const FCMToken = afterHelper.fcm_token;
          console.log("FCM TOKEN:", FCMToken);
          const payload = {
            notification: {
              title: "OnTheMoov",
              body: `${team.name} has requested help!`,
            },
          };
          return admin.messaging().sendToDevice(FCMToken, payload).then(
              (response) => {
                console.log("Successfully sent message:", response);
                return {success: true};
              }).catch((error) => {
            console.log("ERROR ENCOUNTERED:", error);
            return {error: error.code};
          });
        });
      }

      console.log("No message sent, no change detected.");
      return {success: true};
    });

// Listener method for sending push notification when help request accepted
exports.sendListenerPushNotificationRequestAccepted =
    functions.firestore.document(
        "/requests/{reqId}").onUpdate( (snapshot, context) => {
      const beforeReq = snapshot.before.data();
      const afterReq = snapshot.after.data();
      if (beforeReq.status == "Pending" && afterReq.status == "Accepted") {
        const teamRef = admin.firestore().collection("disabled")
            .doc(`${afterReq.requestedUID}`);
        return getDoc(teamRef).then( (team) => {
          const FCMToken = team.fcm_token;
          console.log("FCM TOKEN:", FCMToken);
          const payload = {
            notification: {
              title: "OnTheMoov",
              body: `Your request has been accepted by ${afterReq.acceptedBy}!`,
            },
          };
          return admin.messaging().sendToDevice(FCMToken, payload).then(
              (response) => {
                console.log("Successfully sent message:", response);
                return {success: true};
              }).catch((error) => {
            console.log("ERROR ENCOUNTERED:", error);
            return {error: error.code};
          });
        });
      }

      console.log("No message sent, no change detected.");
      return {success: true};
    });
