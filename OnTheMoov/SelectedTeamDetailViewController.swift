//
//  Project: OnTheMoov
//  Names: David Tang, Alisha Rawal, Trishton Chang, Josh Cho
//  EID: dwt572, ar85943, tbc657, jkc2796
//  Course: CS371L
//
//  SelectedTeamDetailViewController.swift
//  OnTheMoov
//
//  Created by alisha rawal on 10/31/22.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore
import FirebaseFirestoreSwift

class SelectedTeamDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var teamMembers: UITableView!
    @IBOutlet weak var teamImage: UIImageView!
    @IBOutlet weak var teamLabel: UILabel!
    @IBOutlet weak var joinTeamButton: UIButton!
    @IBOutlet weak var teamMembersLabel: UILabel!
    @IBOutlet weak var viewProfileLabel: UIButton!
    let textCellIdentifier = "TextCell"
    
    var teamUID:String = ""
    var memberList: [HelperStudent] = []
    // Firebase database
    let database = Firestore.firestore()
    
    // Sets text labels in view and propagates profile photo, setting dark mode if active
    override func viewWillAppear(_ animated: Bool) {
        let userDefaults = UserDefaults.standard
                if userDefaults.string(forKey: COLOR_BLIND) == ON {
                    overrideUserInterfaceStyle = .dark
                    self.view.backgroundColor = .darkGray
                } else {
                    overrideUserInterfaceStyle = .light
                    self.view.backgroundColor = UIColor(red: CGFloat(255.0/255.0), green: CGFloat(243.0/255.0), blue: CGFloat(226.0/255.0), alpha: 1)
                }
        let firestore = Firestore.firestore()
        let userRef = firestore.collection("disabled").document(teamUID)
        guard let uid = Auth.auth().currentUser?.uid else { return }
        userRef.getDocument(as: Team.self) { result in
         switch result {
            case .success(let team):
             self.teamLabel.text = team.team_name
             print("TEAM.TEAM: ", team.team)
             print("UID: ", uid)
             if team.team.contains(uid) {
                 self.joinTeamButton.isHidden = true
             }
             
            case .failure(let error):
                print("Error decoding team: \(error)")
            }
        }
        memberList = []
        getMembers()
        propogateProfilePhoto()
        
        // update font size
        guard let fontSize = userDefaults.string(forKey: FONT_SIZE) else { return }
        updateLabelFontSize(fontSize: Double(fontSize)!)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        teamMembers.delegate = self
        teamMembers.dataSource = self
        
        // make profile pics circular
        teamImage.layer.masksToBounds = true
        teamImage.layer.borderWidth = 2
        teamImage.layer.borderColor = UIColor.lightGray.cgColor
        teamImage.layer.cornerRadius = (teamImage.image?.size.width)! / 2.0
    }
    
    func updateLabelFontSize(fontSize: Double) {
       // teamMembersLabel.font = teamMembersLabel.font.withSize(fontSize)
    }
    
    // add profile photo for display
    func propogateProfilePhoto() {
        // get the photo url of this user
        let docRef = database.collection("disabled").document(teamUID)
        let field = "profilePhotoURL"
        
        docRef.getDocument(source: .cache) {
            (document, error) in
            if let document = document {
                // get the photo url from firestore
                let profilePhotoURL = document.get(field) as! String
                print("\n\n\n\nFetched profile photo url for this user: \(profilePhotoURL)\n\n\n\n")
                guard let url = URL(string: profilePhotoURL) else { return }
                
                // download profile photo from firestorage and display to image view
                let task = URLSession.shared.dataTask(with: url) {
                    data, _, err in
                    guard let data = data, err == nil else { return }
                    
                    DispatchQueue.main.async {
                        let image = UIImage(data: data)
                        self.teamImage.image = image
                    }
                }
                task.resume()
            } else {
                print("Document does not exist in cache")
            }
        }
    }
    
    func getMembers(){
        let firestore = Firestore.firestore()
        let teamRef = firestore.collection("disabled").document(teamUID)

        teamRef.getDocument(as: Team.self) { result in
         switch result {
            case .success(let team):
             print("Team: \(team)")
             if !team.team.isEmpty {
                 let teamQuery = firestore.collection("helper").whereField("uid", in: team.team)
                 teamQuery.getDocuments() { (querySnapshot, err) in
                     if let err = err {
                         print("Error getting documents: \(err)")
                     } else {
                         for document in querySnapshot!.documents {
                             let member = HelperStudent(snapshot: document)
                             self.memberList.append(member)
                             self.teamMembers.reloadData()
                         }
                     }
                 }
             }
             
            case .failure(let error):
                print("Error decoding team: \(error)")
            }
        }
    }
    
    // Joins team, updating member list, or shows warning alert if already part of team
    @IBAction func joinTeam(_ sender: Any) {
        let firestore = Firestore.firestore()
        guard let uid = Auth.auth().currentUser?.uid else { return }
        let userRef = firestore.collection("helper").document(uid)
        let teamRef = firestore.collection("disabled").document(teamUID)

        userRef.getDocument(as: HelperStudent.self) { result in
         switch result {
            case .success(let user):
             if user.my_team != nil {
                 let controller = UIAlertController(
                     title: "You are already in a team!",
                     message: "Are you sure you would like to leave your current team to join this one?",
                     preferredStyle: .alert)
                 
                 controller.addAction(UIAlertAction(title: "Yes", style: .default, handler: {
                     (action: UIAlertAction!) in
                     let oldTeamRef = firestore.collection("disabled").document(user.my_team!)
                     oldTeamRef.updateData(["team": FieldValue.arrayRemove([uid])])
                     self.memberList.append(user)
                     self.teamMembers.reloadData()
                     self.joinTeamButton.isHidden = true
                     
                     teamRef.getDocument(as: Team.self) { result in
                      switch result {
                         case .success(let team):
                          print("Team: \(team)")
                          teamRef.updateData(["team": FieldValue.arrayUnion([uid])])
                          userRef.updateData(["my_team": self.teamUID])
                          userRef.updateData(["active_request": FieldValue.delete()])
                         case .failure(let error):
                             print("Error decoding team: \(error)")
                         }
                     }
                     
                     // check if potential active request needed to add back in
                     teamRef.getDocument(source: .server) {
                         (document, error) in
                         if let document = document {
                             if let active_request = document.get("active_request") as? String {
                                 // update data in firestore
                                 userRef.updateData(["active_request": active_request])
                             }
                         }
                     }
                 }))
                 controller.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: {
                     (action: UIAlertAction!) in
                     return
                 }))
                 self.present(controller, animated: true)
             } else {
                 self.memberList.append(user)
                 self.teamMembers.reloadData()
                 self.joinTeamButton.isHidden = true
                 
                 teamRef.getDocument(as: Team.self) { result in
                  switch result {
                     case .success(let team):
                      print("Team: \(team)")
                      teamRef.updateData(["team": FieldValue.arrayUnion([uid])])
                      userRef.updateData(["my_team": self.teamUID])

                      
                     case .failure(let error):
                         print("Error decoding team: \(error)")
                     }
                 }
                 
                 // check if potential active request needed to add back in
                 teamRef.getDocument(source: .server) {
                     (document, error) in
                     if let document = document {
                         if let active_request = document.get("active_request") as? String {
                             // update data in firestore
                             userRef.updateData(["active_request": active_request])
                         }
                     }
                 }

             }
            case .failure(let error):
                print("Error decoding user: \(error)")
            }
        }
        
    }
    
    // Count
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return memberList.count
    }
    
    // Cell selection
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = teamMembers.dequeueReusableCell(withIdentifier: textCellIdentifier, for: indexPath as IndexPath)
        let row = indexPath.row
        let name = memberList[row].name
        cell.textLabel?.text = name
        return cell
    }
    
    // Post selection animation
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        teamMembers.deselectRow(at: indexPath, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "HelperProfileSegue",
            let nextVC = segue.destination as? ProfileHelperViewController,
            let idx = teamMembers.indexPathForSelectedRow?.row {
                nextVC.userUID = memberList[idx].uid
        } else if segue.identifier == "DisabledProfileSegue",
              let nextVC = segue.destination as? ProfileDisabledViewController {
              nextVC.teamUID = teamUID
        }
    }
    
}
