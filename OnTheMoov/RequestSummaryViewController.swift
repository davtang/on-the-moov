//
//  Project: OnTheMoov
//  Names: David Tang, Alisha Rawal, Trishton Chang, Josh Cho
//  EID: dwt572, ar85943, tbc657, jkc2796
//  Course: CS371L
//
//  RequestSummaryViewController.swift
//  OnTheMoov
//
//  Created by alisha rawal on 10/31/22.
//

import UIKit
import FirebaseFirestore

class RequestSummaryViewController: UIViewController {

    @IBOutlet weak var timeReqLabel: UILabel!
    @IBOutlet weak var timeCompletedLabel: UILabel!
    @IBOutlet weak var helpedLabel: UILabel!
    @IBOutlet weak var teamLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var fromLabel: UILabel!
    @IBOutlet weak var toLabel: UILabel!
    @IBOutlet weak var photo: UIImageView!
    
    var helpRequest: HelpRequest? = nil
    // Firebase database
    let database = Firestore.firestore()
    
    // Sets text labels in view and propagates profile photo, setting dark mode if active
    override func viewWillAppear(_ animated: Bool) {
        guard let helpReq = helpRequest else {
            return
        }
        let userDefaults = UserDefaults.standard
                if userDefaults.string(forKey: COLOR_BLIND) == ON {
                    overrideUserInterfaceStyle = .dark
                    self.view.backgroundColor = .darkGray
                } else {
                    overrideUserInterfaceStyle = .light
                    self.view.backgroundColor = UIColor(red: CGFloat(255.0/255.0), green: CGFloat(243.0/255.0), blue: CGFloat(226.0/255.0), alpha: 1)
                }

        
        timeReqLabel.text = helpReq.requestedTime
        timeCompletedLabel.text = helpReq.completionTime
        helpedLabel.text = helpReq.requestedBy
        teamLabel.text = helpReq.teamName
        typeLabel.text = helpReq.requestType
        fromLabel.text = helpReq.meetupLoc
        toLabel.text = helpReq.dropoffLoc
        
        propogateProfilePhoto(helpReqUid: helpReq.requestedUID)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // make profile pics circular
        photo.layer.masksToBounds = true
        photo.layer.borderWidth = 2
        photo.layer.borderColor = UIColor.lightGray.cgColor
        photo.layer.cornerRadius = (photo.image?.size.width)! / 2.0
    }
    
    // Add profile photo for display
    func propogateProfilePhoto(helpReqUid: String) {
        // Get the photo url of this user
        print("Helper requested by uid: \(helpReqUid)")
        let docRef = database.collection("disabled").document(helpReqUid)
        let field = "profilePhotoURL"
        
        docRef.getDocument(source: .server) {
            (document, error) in
            if let document = document {
                // get the photo url from firestore
                let profilePhotoURL = document.get(field) as! String
                print("\n\n\n\nFetched profile photo url for this user: \(profilePhotoURL)\n\n\n\n")
                guard let url = URL(string: profilePhotoURL) else { return }
                
                // download profile photo from firestorage and display to image view
                let task = URLSession.shared.dataTask(with: url) {
                    data, _, err in
                    guard let data = data, err == nil else { return }
                    
                    DispatchQueue.main.async {
                        let image = UIImage(data: data)
                        self.photo.image = image
                    }
                }
                task.resume()
            } else {
                print("Document does not exist in cache")
            }
        }
    }
}
