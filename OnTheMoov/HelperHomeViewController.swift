//
//  Project: OnTheMoov
//  Names: David Tang, Alisha Rawal, Trishton Chang, Josh Cho
//  EID: dwt572, ar85943, tbc657, jkc2796
//  Course: CS371L
//
//  HelperHomeViewController.swift
//  OnTheMoov
//
//  Created by alisha rawal on 10/31/22.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore
import FirebaseFirestoreSwift

class HelperHomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var recentRequests: UITableView!
    @IBOutlet weak var activeRequestButton: UIButton!
    @IBOutlet weak var recentRequestsLabel: UILabel!
    @IBOutlet weak var activeRequestsLabel: UILabel!
    var request_uid: String? = nil
    var requestList: [HelpRequest] = []
    let firestore = Firestore.firestore()
    
    // Sets text labels in view and propagates profile photo, setting dark mode if active
    override func viewWillAppear(_ animated: Bool) {
        let userDefaults = UserDefaults.standard
                if userDefaults.string(forKey: COLOR_BLIND) == ON {
                    overrideUserInterfaceStyle = .dark
                    self.view.backgroundColor = .darkGray
                } else {
                    overrideUserInterfaceStyle = .light
                    self.view.backgroundColor = UIColor(red: CGFloat(255.0/255.0), green: CGFloat(243.0/255.0), blue: CGFloat(226.0/255.0), alpha: 1)
                }

        guard let uid = Auth.auth().currentUser?.uid else { return }
        let userRef = firestore.collection("helper").document(uid)
        userRef.getDocument(as: HelperStudent.self) { result in
             switch result {
                case .success(let helper):
                 if helper.active_request != nil {
                     self.activeRequestButton.setTitle("View Active Request", for: .normal)
                     self.request_uid = helper.active_request
                 } else {
                     self.activeRequestButton.setTitle("No Current Active Request", for: .normal)
                 }
                case .failure(let error):
                    print("Error decoding team: \(error)")
            }
        }
        requestList = []
        getRecentRequests()
        
        // Update font size
        guard let fontSize = userDefaults.string(forKey: FONT_SIZE) else { return }
        updateLabelFontSize(fontSize: Double(fontSize)!)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        recentRequests.delegate = self
        recentRequests.dataSource = self
    }
    
    // Updates font size of labels on page
    func updateLabelFontSize(fontSize: Double) {
        recentRequestsLabel.font = recentRequestsLabel.font.withSize(fontSize)
        activeRequestsLabel.font = activeRequestsLabel.font.withSize(fontSize)
    }
    
    // Returns number of requests in tableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return requestList.count
    }
    
    // fills requestList with recently completed requests
    func getRecentRequests() {
        print("GETTING RECENT REQUESTS")
        guard let uid = Auth.auth().currentUser?.uid else { return }
        let requestQuery = firestore.collection("requests").whereField("acceptedUID", in: [uid])
        requestQuery.getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    print("APPENDING REQUEST")
                    let request = HelpRequest(snapshot: document)
                    if request.status == "Complete" {
                        self.requestList.append(request)
                        self.recentRequests.reloadData()
                    }
                }
            }
        }
    }
    
    // Creates cells for recentRequests
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = recentRequests.dequeueReusableCell(withIdentifier: "RecentRequestCell", for: indexPath as IndexPath)
        let row = indexPath.row
        let meetup = requestList[row].meetupLoc
        let dest = requestList[row].dropoffLoc
        let completeTime = requestList[row].completionTime!
        cell.textLabel?.text = "\(meetup) to \(dest) at \(completeTime)"
        return cell
    }
    
    
    // Deselects row after selection animation
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        recentRequests.deselectRow(at: indexPath, animated: true)
    }

    // Segues to active request or shows alert indicating no current requests active
    @IBAction func viewActiveRequest(_ sender: Any) {
        if request_uid == nil {
            let controller = UIAlertController(
                title: "There are no current active requests",
                message: "Wait for your team to request help to get On the Moov!",
                preferredStyle: .alert)
            
            controller.addAction(UIAlertAction(title: "OK", style: .default))
            present(controller, animated: true)
        } else {
            let requestRef = firestore.collection("requests").document(request_uid!)
            requestRef.getDocument(as: HelpRequest.self) { result in
                switch result {
                case .success(let request):
                    print("VIEW ACTIVE REQUEST STATUS:", request.status)
                    if request.status == "Pending" {
                        self.performSegue(withIdentifier: "RequestDetailedSegue", sender: request)
                    } else if request.status == "Accepted" {
                        self.performSegue(withIdentifier: "AcceptedRequestSegue", sender: request)
                    }

                case .failure(let error):
                    print("Error decoding request: \(error)")
                }
            }
        }
    }
    
    // Passes request to appropriate view controllers
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "RequestDetailedSegue" {
            let nextVC = segue.destination as! RecentRequestDetailedViewController
            let request = sender as! HelpRequest
            nextVC.helpRequest = request
        } else if segue.identifier == "AcceptedRequestSegue" {
            let nextVC = segue.destination as! ActiveRequestDetailViewController
            let request = sender as! HelpRequest
            nextVC.helpRequest = request
        } else if segue.identifier == "RecentRequestSegue",
            let nextVC = segue.destination as? RequestSummaryViewController,
            let requestIdx = recentRequests.indexPathForSelectedRow?.row {
            let selectedReq = requestList[requestIdx]
            nextVC.helpRequest = selectedReq
        }
    }

}
