//
//  Project: OnTheMoov
//  Names: David Tang, Alisha Rawal, Trishton Chang, Josh Cho
//  EID: dwt572, ar85943, tbc657, jkc2796
//  Course: CS371L
//
//  SettingsHelperViewController.swift
//  OnTheMoov
//
//  Created by Joshua Cho on 11/3/22.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore


// settings constants
let COLOR_BLIND = "colorBlind"
let FONT_SIZE = "fontSize"
let ON = "On"
let OFF = "Off"
let PX_16 = "16.0"
let PX_20 = "20.0"
let PX_24 = "24.0"


class SettingsHelperViewController: UIViewController, UNUserNotificationCenterDelegate {

    @IBOutlet weak var colorBlindSegmentControl: UISegmentedControl!
    @IBOutlet weak var fontSizeSegmentControl: UISegmentedControl!
    @IBOutlet weak var profilePhoto: UIImageView!
    @IBOutlet weak var darkModeLabel: UILabel!
    @IBOutlet weak var fontSizeLabel: UILabel!
    
    // User defaults reference
    let userDefaults = UserDefaults.standard
    
    // Firestore database
    let database = Firestore.firestore()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // make profile pics circular
        profilePhoto.layer.masksToBounds = true
        profilePhoto.layer.borderWidth = 2
        profilePhoto.layer.borderColor = UIColor.lightGray.cgColor
        profilePhoto.layer.cornerRadius = (profilePhoto.image?.size.width)! / 2.0
        
        // update storyboard with the right settings
        updateColorBlind()
        updateFontSize()
    }
    
    // Propagates profile photo, setting dark mode if active
    override func viewWillAppear(_ animated: Bool) {
        let userDefaults = UserDefaults.standard
                if userDefaults.string(forKey: COLOR_BLIND) == ON {
                    overrideUserInterfaceStyle = .dark
                    self.view.backgroundColor = .darkGray
                } else {
                    overrideUserInterfaceStyle = .light
                    self.view.backgroundColor = UIColor(red: CGFloat(255.0/255.0), green: CGFloat(243.0/255.0), blue: CGFloat(226.0/255.0), alpha: 1)
                } 
        updateProfilePhoto()
        
        // Update font size
        guard let fontSize = userDefaults.string(forKey: FONT_SIZE) else { return }
        updateLabelFontSize(fontSize: Double(fontSize)!)
    }
    
    // Updates font on screen
    func updateLabelFontSize(fontSize: Double) {
        darkModeLabel.font = darkModeLabel.font.withSize(fontSize)
        fontSizeLabel.font = fontSizeLabel.font.withSize(fontSize)
    }
    
    // Retrieves current user UID
    private func getCurrentUserUID() -> String {
        guard let uid = Auth.auth().currentUser?.uid else {
            print("\n\n\nCOULD NOT RETREIVE USER ID\n\n")
            return "NO_USER_ID"
        }
        return uid
    }
    
    // Updates user profile photos
    private func updateProfilePhoto() {
        // get the photo url of this user
        let docRef = database.collection("helper").document(getCurrentUserUID())
        let field = "profilePhotoURL"
        
        docRef.getDocument(source: .cache) {
            (document, error) in
            if let document = document {
                // get the photo url from firestore
                let profilePhotoURL = document.get(field) as! String
                print("\n\n\n\nFetched profile photo url for this user: \(profilePhotoURL)\n\n\n\n")
                guard let url = URL(string: profilePhotoURL) else { return }
                
                // download profile photo from firestorage and display to image view
                let task = URLSession.shared.dataTask(with: url) {
                    data, _, err in
                    guard let data = data, err == nil else { return }
                    
                    DispatchQueue.main.async {
                        let image = UIImage(data: data)
                        self.profilePhoto.image = image
                    }
                }
                task.resume()
            } else {
                print("Document does not exist in cache")
            }
        }
    }
    
    // Update the color blind segment control
    private func updateColorBlind() {
        let colorBlind = userDefaults.string(forKey: COLOR_BLIND)
        if colorBlind == OFF {
            colorBlindSegmentControl.selectedSegmentIndex = 0
            // do work here for notifs
        } else if colorBlind == ON {
            colorBlindSegmentControl.selectedSegmentIndex = 1
        }
    }
    
    // Update the font size segment control
    private func updateFontSize() {
        let fontSize = userDefaults.string(forKey: FONT_SIZE)
        if fontSize == PX_16 {
            fontSizeSegmentControl.selectedSegmentIndex = 0
            // update font size
            guard let fontSize = userDefaults.string(forKey: FONT_SIZE) else { return }
            updateLabelFontSize(fontSize: Double(fontSize)!)
        } else if fontSize == PX_20 {
            fontSizeSegmentControl.selectedSegmentIndex = 1
            // update font size
            guard let fontSize = userDefaults.string(forKey: FONT_SIZE) else { return }
            updateLabelFontSize(fontSize: Double(fontSize)!)
        } else if fontSize == PX_24 {
            fontSizeSegmentControl.selectedSegmentIndex = 2
            // update font size
            guard let fontSize = userDefaults.string(forKey: FONT_SIZE) else { return }
            updateLabelFontSize(fontSize: Double(fontSize)!)
        }
    }
    
    // Updates colorblind segment control depending on selected segment
    @IBAction func colorBlindSegmentChanged(_ sender: Any) {
        switch colorBlindSegmentControl.selectedSegmentIndex {
        case 0:
            userDefaults.set(OFF, forKey: COLOR_BLIND)
            self.viewWillAppear(true)
        case 1:
            userDefaults.set(ON, forKey: COLOR_BLIND)
            self.viewWillAppear(true)
        default:
            userDefaults.set(OFF, forKey: COLOR_BLIND)
        }
        // update storyboard with right setting
        updateColorBlind()
    }
    
    // Updates font size segment control depending on selected segment
    @IBAction func fontSizeSegmentChanged(_ sender: Any) {
        switch fontSizeSegmentControl.selectedSegmentIndex {
        case 0:
            userDefaults.set(PX_16, forKey: FONT_SIZE)
        case 1:
            userDefaults.set(PX_20, forKey: FONT_SIZE)
        case 2:
            userDefaults.set(PX_24, forKey: FONT_SIZE)
        default:
            userDefaults.set(PX_16, forKey: FONT_SIZE)
        }
        // update storyboard with right setting
        updateFontSize()
    }
    
    // Logs out user
    @IBAction func logoutButtonPressed(_ sender: Any) {
        do {
            try Auth.auth().signOut()
            performSegue(withIdentifier: "HelperLogoutSegue", sender: nil)
        } catch {
            print("Sign out error")
        }
    }
}
