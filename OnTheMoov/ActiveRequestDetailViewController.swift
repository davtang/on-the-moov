//
//  Project: OnTheMoov
//  Names: David Tang, Alisha Rawal, Trishton Chang, Josh Cho
//  EID: dwt572, ar85943, tbc657, jkc2796
//  Course: CS371L
//
//  ActiveRequestDetailViewController.swift
//  OnTheMoov
//
//  Created by alisha rawal on 10/31/22.
//

import MapKit
import CoreLocation
import UIKit
import FirebaseAuth
import FirebaseFirestore

class ActiveRequestDetailViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var pronounsLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var fromLabel: UILabel!
    @IBOutlet weak var toLabel: UILabel!
    @IBOutlet weak var dropoffLabel: UILabel!
    
    var locationManager = CLLocationManager()
    var helpRequest: HelpRequest? = nil
    // Firebase database
    let database = Firestore.firestore()
    
    // Sets text labels in view and propagates profile photo, setting dark mode if active
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let helpReq = helpRequest else {
            return
        }
        let userDefaults = UserDefaults.standard
                if userDefaults.string(forKey: COLOR_BLIND) == ON {
                    overrideUserInterfaceStyle = .dark
                    self.view.backgroundColor = .darkGray
                } else {
                    overrideUserInterfaceStyle = .light
                    self.view.backgroundColor = UIColor(red: CGFloat(255.0/255.0), green: CGFloat(243.0/255.0), blue: CGFloat(226.0/255.0), alpha: 1)
                }

        
        nameLabel.text = helpReq.requestedBy
        pronounsLabel.text = helpReq.reqPronouns
        typeLabel.text = helpReq.requestType
        fromLabel.text = helpReq.meetupLoc
        toLabel.text = helpReq.dropoffLoc
        dropoffLabel.text = helpReq.dropoffTime
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        mapView.delegate = self
        
        getAddress()
        propogateProfilePhoto(helpReqUid: helpReq.requestedUID)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // make profile pics circular
        photo.layer.masksToBounds = true
        photo.layer.borderWidth = 2
        photo.layer.borderColor = UIColor.lightGray.cgColor
        photo.layer.cornerRadius = (photo.image?.size.width)! / 2.0
    }
    
    // Obtain meetup location
    func getAddress() {
        let geoCoder = CLGeocoder()
        
        geoCoder.geocodeAddressString(fromLabel.text!){ (placemarks, error) in
            guard let placemarks = placemarks, let location = placemarks.first?.location
            else {
                print("No location found")
                return
            }
            self.mapThis(destinationCoord: location.coordinate)
        }
    }
    
    // Maps current coordinates on map
    func mapThis(destinationCoord: CLLocationCoordinate2D){
        let sourceCoordinate = (locationManager.location?.coordinate)!
        
        let sourcePlacemark = MKPlacemark(coordinate: sourceCoordinate)
        let destPlacemark = MKPlacemark(coordinate: destinationCoord)
        
        let sourceItem = MKMapItem(placemark: sourcePlacemark)
        let destItem = MKMapItem(placemark: destPlacemark)
        
        let destinationRequest = MKDirections.Request()
        destinationRequest.source = sourceItem
        destinationRequest.destination = destItem
        destinationRequest.transportType = .walking
        destinationRequest.requestsAlternateRoutes = true
        
        let directions = MKDirections(request: destinationRequest)
        directions.calculate { (response, error) in
            guard let response = response else {
                if let error = error {
                    print("Error")
                }
                return
            }
            let route = response.routes[0]
            self.mapView.addOverlay(route.polyline)
            self.mapView.setVisibleMapRect(route.polyline.boundingMapRect, animated: true)
        }
    }
    
    // Renders the path on map
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let render = MKPolylineRenderer(overlay: overlay as! MKPolyline)
        render.strokeColor = .blue
        return render
    }
    
    // Add profile photo for display
    func propogateProfilePhoto(helpReqUid: String) {
        // Get the photo url of this user
        let docRef = database.collection("disabled").document(helpReqUid)
        let field = "profilePhotoURL"
        
        docRef.getDocument(source: .cache) {
            (document, error) in
            if let document = document {
                // Get the photo url from firestore
                let profilePhotoURL = document.get(field) as! String
                print("\n\n\n\nFetched profile photo url for this user: \(profilePhotoURL)\n\n\n\n")
                guard let url = URL(string: profilePhotoURL) else { return }
                
                // Download profile photo from firestorage and display to image view
                let task = URLSession.shared.dataTask(with: url) {
                    data, _, err in
                    guard let data = data, err == nil else { return }
                    
                    DispatchQueue.main.async {
                        let image = UIImage(data: data)
                        self.photo.image = image
                    }
                }
                task.resume()
            } else {
                print("Document does not exist in cache")
            }
        }
    }
    
    // Formats time into short format
    func formatTime(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        formatter.dateStyle = .none
        return formatter.string(from: date)
    }
    
    // Logs completed request and updates all involved user profiles
    @IBAction func completeButton(_ sender: Any) {
        guard let helpReq = helpRequest else {
            return
        }
        let firestore = Firestore.firestore()
        let batch = firestore.batch()
        
        
        guard let uid = Auth.auth().currentUser?.uid else { return }
        let userRef = firestore.collection("helper").document(uid)
        let requesterRef = firestore.collection("disabled").document(helpReq.requestedUID)
        let requestRef = firestore.collection("requests").document(helpReq.uid!)
        
        let currentTime = Date()
        
        batch.updateData(["status": "Complete", "completionTime": formatTime(date:currentTime)], forDocument: requestRef)
        batch.updateData([
            "active_request": FieldValue.delete()], forDocument: userRef)
        batch.updateData([
            "active_request": FieldValue.delete()], forDocument: requesterRef)
        batch.updateData(["completed_requests": FieldValue.arrayUnion([helpReq.uid!])], forDocument: userRef)
        batch.updateData(["completed_requests": FieldValue.arrayUnion([helpReq.uid!])], forDocument: requesterRef)
        
        batch.commit() { err in
            if let err = err {
                print("Error writing batch \(err)")
            } else {
                print("Batch write succeeded.")
            }
        }
        
        helpReq.completionTime = formatTime(date:currentTime)
        
        performSegue(withIdentifier: "CompleteRequestSegue", sender: helpReq)
    }
    
    // Passes current active request to RequestSummaryViewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CompleteRequestSegue" {
            let nextVC = segue.destination as! RequestSummaryViewController
            let request = sender as! HelpRequest

            nextVC.helpRequest = request
        }
    }

}
