//
//  Project: OnTheMoov
//  Names: David Tang, Alisha Rawal, Trishton Chang, Josh Cho
//  EID: dwt572, ar85943, tbc657, jkc2796
//  Course: CS371L
//
//  LoginViewController.swift
//  OnTheMoov
//
//  Created by David Tang on 10/13/22.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore

class LoginViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var emailField: UITextField!
    
    @IBOutlet weak var passwordField: UITextField!
    
    @IBOutlet weak var errorMessage: UILabel!
    
    @IBOutlet weak var registerButton: UIButton!
    
    // Shows dark mode if active
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let userDefaults = UserDefaults.standard
                if userDefaults.string(forKey: COLOR_BLIND) == ON {
                    overrideUserInterfaceStyle = .dark
                    self.view.backgroundColor = .darkGray
                } else {
                    overrideUserInterfaceStyle = .light
                    self.view.backgroundColor = UIColor(red: CGFloat(255.0/255.0), green: CGFloat(243.0/255.0), blue: CGFloat(226.0/255.0), alpha: 1)
                }

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emailField.delegate = self
        passwordField.delegate = self
        
        // Set color for button
        let darkOrange = UIColor(red: CGFloat(181.0/255.0), green: CGFloat(103.0/255), blue: CGFloat(36.0/255), alpha: 1)
        let font = UIFont(name: "Helvetica Neue Bold", size: 15.0)
        let attributes: [NSAttributedString.Key: Any] = [
            .font: font!,
            .foregroundColor: darkOrange,
        ]
        let attributedString = NSAttributedString(string: "GET ON THE MOOV!", attributes: attributes)
        registerButton.setAttributedTitle(attributedString, for: .normal)
        registerButton.titleLabel?.textAlignment = .center
        

        Auth.auth().addStateDidChangeListener() {
            auth, user in
            if user != nil {
                let firestore = Firestore.firestore()
                guard let uid = Auth.auth().currentUser?.uid else { return }
                let docRef = firestore.collection("disabled").document(uid)
                docRef.getDocument { (document, error) in
                    if let document = document, document.exists {
                        self.performSegue(withIdentifier: "DisabledLoginSegue", sender: nil)
                    } else {
                        self.performSegue(withIdentifier: "HelperLoginSegue", sender: nil)
                    }
                }
                self.emailField.text = nil
                self.passwordField.text = nil
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // Dismisses keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // Signs in this user with credentials stored in Firebase
    @IBAction func submitButtonPressed(_ sender: Any) {
        Auth.auth().signIn(withEmail: emailField.text!, password: passwordField.text!) {
            authResult, error in 
            if let error = error as NSError? {
                self.errorMessage.text = "\(error.localizedDescription)"
            } else {
                self.errorMessage.text = ""
            }
        }
    }
    
}
