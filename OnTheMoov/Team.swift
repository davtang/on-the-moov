//
//  Project: OnTheMoov
//  Names: David Tang, Alisha Rawal, Trishton Chang, Josh Cho
//  EID: dwt572, ar85943, tbc657, jkc2796
//  Course: CS371L
//
//  Team.swift
//  OnTheMoov
//
//  Created by David Tang on 10/18/22.
//

import FirebaseFirestore

// Represents a disabled student, aligns with Collection on Firestore
struct Team: Codable{
    var team_name: String
    var name: String
    var pronouns: String
    var year: String
    var bio: String
    var email: String
    var uid: String
    var team: [String]
    var fcm_token: String
    var active_request: String?
    var completed_requests: [String]?
    var profilePhotoURL: String
        
    init(teamName: String, name: String, pronouns: String, year: String, bio: String, email:String, uid: String, fcmToken: String, profilePhotoURL: String) {
        self.team_name = teamName
        self.name = name
        self.pronouns = pronouns
        self.year = year
        self.bio = bio
        self.email = email
        self.uid = uid
        self.fcm_token = fcmToken
        self.team = []
        self.active_request = nil
        self.profilePhotoURL = profilePhotoURL
    }
    
    init(snapshot: DocumentSnapshot) {
        let snapshotValue = snapshot.data()!
        self.team_name = snapshotValue["team_name"] as! String
        self.name = snapshotValue["name"] as! String
        self.pronouns = snapshotValue["pronouns"] as! String
        self.year = snapshotValue["year"] as! String
        self.bio = snapshotValue["bio"] as! String
        self.email = snapshotValue["email"] as! String
        self.uid = snapshotValue["uid"] as! String
        self.fcm_token = snapshotValue["fcm_token"] as! String
        self.team = snapshotValue["team"] as! [String]
        self.active_request = snapshotValue["active_request"] as? String
        self.completed_requests = snapshotValue["completed_requests"] as? [String]
        self.profilePhotoURL = snapshotValue["profilePhotoURL"] as! String
    }
    
}
