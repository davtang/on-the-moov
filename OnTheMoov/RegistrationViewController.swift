//
//  Project: OnTheMoov
//  Names: David Tang, Alisha Rawal, Trishton Chang, Josh Cho
//  EID: dwt572, ar85943, tbc657, jkc2796
//  Course: CS371L
//
//  RegistrationViewController.swift
//  OnTheMoov
//
//  Created by David Tang on 10/13/22.
//

import UIKit

class RegistrationViewController: UIViewController {
    
    @IBOutlet weak var createDisabledProfile: UIButton!
    
    @IBOutlet weak var createStudentProfile: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Format buttons
        let font1 = UIFont(name: "Helvetica Neue Bold", size: 30.0)
        let font2 = UIFont(name: "Helvetica Neue", size: 15.0)
        let darkOrange = UIColor(red: CGFloat(181.0/255.0), green: CGFloat(103.0/255), blue: CGFloat(36.0/255), alpha: 1)

        // Create attributes for large and small text
        let attributesLarge: [NSAttributedString.Key: Any] = [
            .font: font1!,
            .foregroundColor: darkOrange,
        ]
        let attributesSmall: [NSAttributedString.Key: Any] = [
            .font: font2!,
            .foregroundColor: UIColor.white,
        ]
        
        // Set disabled button text
        let attributedDisabled1 = NSMutableAttributedString(string: "I am a disabled student and want to", attributes: attributesSmall)
        let attributedDisabled2 = NSAttributedString(string: "\nCREATE A TEAM", attributes: attributesLarge)
        attributedDisabled1.append(attributedDisabled2)
        
        createDisabledProfile.setAttributedTitle(attributedDisabled1, for: .normal)
        createDisabledProfile.titleLabel?.textAlignment = .center
        
        // Set student helper button text
        let attributedHelper1 = NSMutableAttributedString(string: "I am a student helper and want to", attributes: attributesSmall)
        let attributedHelper2 = NSAttributedString(string: "\nJOIN A TEAM", attributes: attributesLarge)
        attributedHelper1.append(attributedHelper2)
        
        createStudentProfile.setAttributedTitle(attributedHelper1, for: .normal)
        createStudentProfile.titleLabel?.textAlignment = .center
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let userDefaults = UserDefaults.standard
                if userDefaults.string(forKey: COLOR_BLIND) == ON {
                    overrideUserInterfaceStyle = .dark
                    self.view.backgroundColor = .darkGray
                } else {
                    overrideUserInterfaceStyle = .light
                    self.view.backgroundColor = UIColor(red: CGFloat(255.0/255.0), green: CGFloat(243.0/255.0), blue: CGFloat(226.0/255.0), alpha: 1)
                }

    }

    // Activates Segue while passing Bool indicating this is a Disabled Student
    @IBAction func disabledStudentButtonPressed(_ sender: Any) {
        self.performSegue(withIdentifier: "CreateProfileSegue", sender: true)
    }
    
    // Activates Segue while passing Bool indicating this is a Student Helper
    @IBAction func helperStudentButtonPressed(_ sender: Any) {
        self.performSegue(withIdentifier: "CreateProfileSegue", sender: false)
    }
    
    // Parses Bool and passes ProfileCreationVC to alter labels
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "CreateProfileSegue") {
            let nextVC = segue.destination as! ProfileCreationViewController
            let isDisabled = sender as! Bool
            nextVC.isDisabledStudent = isDisabled
        }
    }
}
