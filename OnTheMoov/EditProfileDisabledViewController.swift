//
//  Project: OnTheMoov
//  Names: David Tang, Alisha Rawal, Trishton Chang, Josh Cho
//  EID: dwt572, ar85943, tbc657, jkc2796
//  Course: CS371L
//
//  EditProfileDisabledViewController.swift
//  OnTheMoov
//
//  Created by alisha rawal on 10/31/22.
//

import UIKit
import Foundation
import FirebaseFirestore
import FirebaseStorage
import FirebaseAuth
import FirebaseMessaging



class EditProfileDisabledViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var bioField: UITextField!
    @IBOutlet weak var pronounsField: UITextField!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var yearCtrl: UISegmentedControl!
    var year:String = "First"
    
    // Firebase database
    let database = Firestore.firestore()
 
    // Sets dark mode if active
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let userDefaults = UserDefaults.standard
                if userDefaults.string(forKey: COLOR_BLIND) == ON {
                    overrideUserInterfaceStyle = .dark
                    self.view.backgroundColor = .darkGray
                } else {
                    overrideUserInterfaceStyle = .light
                    self.view.backgroundColor = UIColor(red: CGFloat(255.0/255.0), green: CGFloat(243.0/255.0), blue: CGFloat(226.0/255.0), alpha: 1)
                }

    }
    
    // Propagates textfields with user settings
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        bioField.delegate = self
        pronounsField.delegate = self
        nameField.delegate = self
        
        // make profile pics circular
        profilePic.layer.masksToBounds = true
        profilePic.layer.borderWidth = 2
        profilePic.layer.borderColor = UIColor.lightGray.cgColor
        profilePic.layer.cornerRadius = (profilePic.image?.size.width)! / 2.0
        
        let uid:String = getCurrentUserUID()
        updateProfilePhoto(uid: uid)
        updateName()
        updatePronouns()
        updateYear()
        updateBio()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // removes keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // Gets current UID
    private func getCurrentUserUID() -> String {
        guard let uid = Auth.auth().currentUser?.uid else {
            print("\n\n\nCOULD NOT RETREIVE USER ID\n\n")
            return "NO_USER_ID"
        }
        return uid
    }
    
    // Update the profile photo with user photo in cloud storage (or keep default)
    private func updateProfilePhoto(uid:String) {
        // Get the photo url of this user
        let docRef = database.collection("disabled").document(getCurrentUserUID())
        let field = "profilePhotoURL"
        
        docRef.getDocument(source: .cache) {
            (document, error) in
            if let document = document {
                // get the photo url from firestore
                let profilePhotoURL = document.get(field) as! String
                print("\n\n\n\nFetched profile photo url for this user: \(profilePhotoURL)\n\n\n\n")
                guard let url = URL(string: profilePhotoURL) else { return }
                
                // download profile photo from firestorage and display to image view
                let task = URLSession.shared.dataTask(with: url) {
                    data, _, err in
                    guard let data = data, err == nil else { return }
                    
                    DispatchQueue.main.async {
                        let image = UIImage(data: data)
                        self.profilePic.image = image
                    }
                }
                task.resume()
            } else {
                print("Document does not exist in cache")
            }
        }
    }
    
    // Updates name of user in Firestore
    func updateName() {
        let docRef = database.collection("disabled").document(getCurrentUserUID())
        let field = "name"
        
        docRef.getDocument(source: .cache) {
            (document, error) in
            if let document = document {
                let property = document.get(field) as! String
                
                DispatchQueue.main.async {
                    self.nameField.text = property
                }
            } else {
                print("Document does not exist in cache")
            }
        }
    }
    
    // Updates pronouns of user in Firestore
    func updatePronouns() {
        let docRef = database.collection("disabled").document(getCurrentUserUID())
        let field = "pronouns"
        
        docRef.getDocument(source: .cache) {
            (document, error) in
            if let document = document {
                let property = document.get(field) as! String
                
                DispatchQueue.main.async {
                    self.pronounsField.text = property
                }
            } else {
                print("Document does not exist in cache")
            }
        }
    }
    
    // Updates year of user in Firestore
    func updateYear() {
        let docRef = database.collection("disabled").document(getCurrentUserUID())
        let field = "year"
        
        docRef.getDocument(source: .cache) {
            (document, error) in
            if let document = document {
                let property = document.get(field) as! String
                
                DispatchQueue.main.async {
                    self.year = property
                    if property == "First" {
                        self.yearCtrl.selectedSegmentIndex = 0
                    } else if property == "Second" {
                        self.yearCtrl.selectedSegmentIndex = 1
                    } else if property == "Third" {
                        self.yearCtrl.selectedSegmentIndex = 2
                    } else if property == "Fourth" {
                        self.yearCtrl.selectedSegmentIndex = 3
                    } else {
                        self.yearCtrl.selectedSegmentIndex = 4
                    }
                }
            } else {
                print("Document does not exist in cache")
            }
        }
    }
    
    // Updates year if segment changed
    @IBAction func onYearSegmentChanged(_ sender: Any) {
        switch yearCtrl.selectedSegmentIndex {
        case 0:
            year = "First"
        case 1:
            year = "Second"
        case 2:
            year = "Third"
        case 3:
            year = "Fourth"
        case 4:
            year = "Other"
        default:
            year = "Invalid"
        }
    }
    
    // Updates bio of users in Firestore
    func updateBio() {
        let docRef = database.collection("disabled").document(getCurrentUserUID())
        let field = "bio"
        
        docRef.getDocument(source: .cache) {
            (document, error) in
            if let document = document {
                let property = document.get(field) as! String
                
                DispatchQueue.main.async {
                    self.bioField.text = property
                }
            } else {
                print("Document does not exist in cache")
            }
        }
    }
    
    // Photo selection
    @IBAction func changePhoto(_ sender: Any) {
        presentPhotoActionSheet()
    }
    
    // Saves user changes
    @IBAction func saveButtonPressed(_ sender: Any) {
        persistImageToStorage()
        let controller = UIAlertController(
            title: "Changes saved!",
            message: "Updated profile settings",
            preferredStyle: .alert)
        
        controller.addAction(UIAlertAction(title: "OK",
                                           style: .default))
        present(controller, animated: true)
    }
    
    // Stores user info into firestore
    func storeUserInformation(profilePhotoURL:String){
        let uid = getCurrentUserUID()
        let docRef = database.collection("disabled").document(getCurrentUserUID())
        
        docRef.getDocument(source: .cache) {
            (document, error) in
            if error == nil {
                // update data in firestore
                self.database.collection("disabled").document(uid).updateData([
                        "name": self.nameField.text!,
                        "pronouns": self.pronounsField.text!,
                        "year": self.year,
                        "bio": self.bioField.text!,
                        "profilePhotoURL": profilePhotoURL
                ])
            } else {
                print("Document does not exist in cache")
            }
        }
    }
}

// Extension for profile photo capability
extension EditProfileDisabledViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func presentPhotoActionSheet() {
        let actionSheet = UIAlertController(title: "Profile Picture",
                                            message: "How would you like to select a picture?",
                                            preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Cancel",
                                            style: .cancel))
        actionSheet.addAction(UIAlertAction(title: "Take Photo",
                                            style: .default) {
            [weak self] _ in
            self?.presentCamera()
        })
        actionSheet.addAction(UIAlertAction(title: "Choose From Gallery",
                                            style: .default) {
            [weak self] _ in
            self?.presentPhotoPicker()
        })
        
        // show to user now
        present(actionSheet, animated: true)
    }
    
    // let user take camera photo for profile
    func presentCamera() {
        let vc = UIImagePickerController()
        vc.sourceType = .camera
        vc.delegate = self
        vc.allowsEditing = true  // let user crop the photo
        present(vc, animated: true)
    }
    
    // let user choose photo for profile
    func presentPhotoPicker() {
        let vc = UIImagePickerController()
        vc.sourceType = .photoLibrary
        vc.delegate = self
        vc.allowsEditing = true  // let user crop the photo
        present(vc, animated: true)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        guard let selectedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else {
            return
        }
        self.profilePic.image = selectedImage
    }
    
    // Store image into firebase storage
    func persistImageToStorage() {
        guard let uid = Auth.auth().currentUser?.uid else {
            return
        }
        let ref = Storage.storage().reference(withPath: uid)
        
        // make the profile photo into a jpeg
        guard let imageData = self.profilePic.image?.jpegData(compressionQuality: 0.5) else {
            return
        }
        
        // put into storage
        ref.putData(imageData) {
            metadata, err in
            if let err = err {
                print("Error in putting profile photo into firebase storage, \(err)")
                return
            }
            ref.downloadURL {
                url, err in
                if let err = err {
                    print("Error in retrieving download URL, \(err)")
                    return
                }
                print("Succesfully stored photo into firebase storage with url: \(url?.absoluteString ?? "")")
                
                // now store to firestore userdata
                let profilePhotoURL:String = url?.absoluteString ?? ""
                self.storeUserInformation(profilePhotoURL: profilePhotoURL)
            }
        }
    }
}

