//
//  Project: OnTheMoov
//  Names: David Tang, Alisha Rawal, Trishton Chang, Josh Cho
//  EID: dwt572, ar85943, tbc657, jkc2796
//  Course: CS371L
//
//  CreateRequestDetailedViewController.swift
//  OnTheMoov
//
//  Created by alisha rawal on 10/31/22.
//

import CoreLocation
import MapKit
import UIKit
import FirebaseFirestore
import FirebaseAuth

class CreateRequestDetailedViewController: UIViewController, UITextFieldDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var requestTypeSeg: UISegmentedControl!
    @IBOutlet weak var dropoffTime: UITextField!
    @IBOutlet weak var dropoffLoc: UITextField!
    @IBOutlet weak var meetupLoc: UITextField!
    @IBOutlet weak var currLocation: MKMapView!
    
    @IBOutlet weak var errorMessage: UILabel!
    var requestType = "Escort"
    let firestore = Firestore.firestore()
    let manager = CLLocationManager()
    
    // Propagates location of the user
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.delegate = self
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
    }
    
    // Freezes first location and renders
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first{
            manager.stopUpdatingLocation()
            render(location)
            getAddress(location)
        }
    }
    
    // Renders current location on map
    func render(_ location: CLLocation){
        let coordinate = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        
        let span = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
        
        let region = MKCoordinateRegion(center: coordinate, span: span)
        currLocation.setRegion(region,
                               animated: true)
        
        let pin = MKPointAnnotation()
        pin.coordinate = coordinate
        currLocation.addAnnotation(pin)
    }
    
    // Converts location to address
    func getAddress(_ location: CLLocation){
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(location) { (placemarks, error) in
            if let error = error {
                return
            }
            else if let placemark = placemarks?.first {
                let address = [placemark.name,
                               placemark.subThoroughfare,
                               placemark.thoroughfare,
                               placemark.locality,
                               placemark.subAdministrativeArea,
                               placemark.administrativeArea].compactMap({$0}).joined(separator: ", ")
                self.meetupLoc.text = address
            }
        }
    }
    
    // Applies night mode if active
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let userDefaults = UserDefaults.standard
                if userDefaults.string(forKey: COLOR_BLIND) == ON {
                    overrideUserInterfaceStyle = .dark
                    self.view.backgroundColor = .darkGray
                } else {
                    overrideUserInterfaceStyle = .light
                    self.view.backgroundColor = UIColor(red: CGFloat(255.0/255.0), green: CGFloat(243.0/255.0), blue: CGFloat(226.0/255.0), alpha: 1)
                }

    }
    
    // Enables date picker for time on request creation
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        dropoffTime.delegate = self
        dropoffLoc.delegate = self
        meetupLoc.delegate = self
        
        let timePicker = UIDatePicker()
        timePicker.datePickerMode = .time
        timePicker.addTarget(self, action: #selector(timeChange(timePicker:)), for: UIControl.Event.valueChanged)
        timePicker.preferredDatePickerStyle = .wheels
        
        dropoffTime.inputView = timePicker
    }
    
    @objc func timeChange(timePicker: UIDatePicker) {
        dropoffTime.text = formatTime(date: timePicker.date)
    }
    
    // Helper function to format time into short format
    func formatTime(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        formatter.dateStyle = .none
        return formatter.string(from: date)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // Ends editing on textfield
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // Changes requestType based on selected segment
    @IBAction func onReqSegmentChanged(_ sender:Any) {
        switch requestTypeSeg.selectedSegmentIndex {
        case 0: requestType = "Escort"
        case 1: requestType = "Task"
        case 2: requestType = "Meal"
        default:
            requestType = "Invalid"
        }
    }
    
    // Creates request if all fields are filled out, updates all involved users
    @IBAction func confirmButton(_ sender: Any) {
        guard let dTime = dropoffTime.text,
              let dLoc = dropoffLoc.text,
              let mLoc = meetupLoc.text,
              !dTime.isEmpty && !dLoc.isEmpty &&
              !mLoc.isEmpty else {
            errorMessage.text = "Please fill out all fields above!"
            return
        }
        
        
        
        // Add request
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        let batch = firestore.batch()
        
        let userRef = firestore.collection("disabled").document(uid)
        userRef.getDocument(as: Team.self) { result in
            switch result {
            case .success(let team):
                print("Team: \(team)")
                let currentDateTime = Date()
                let newRequest = HelpRequest(reqType: self.requestType, doTime: dTime, reqTime: self.formatTime(date:currentDateTime), doLoc: dLoc, muLoc: mLoc, requester: team.name, requesterUID: team.uid, pronouns: team.pronouns, team: team.team_name)
                var newRequestRef: DocumentReference? = nil
                do {
                    newRequestRef = try self.firestore.collection("requests").addDocument(from: newRequest)
                    let newRequestId = newRequestRef!.documentID
                    for member in team.team {
                        let memberRef = self.firestore.collection("helper").document(member)
                        batch.updateData(["active_request": newRequestId], forDocument: memberRef)
                    }
                    batch.updateData(["active_request": newRequestId], forDocument: userRef)
                    batch.updateData(["uid": newRequestId], forDocument: newRequestRef!)
                    newRequest.setUID(uid: uid)
                    batch.commit() { err in
                        if let err = err {
                            print("Error writing batch \(err)")
                        } else {
                            print("Batch write succeeded.")
                        }
                    }
                    self.performSegue(withIdentifier: "CreateRequestSegue", sender: newRequest)
                } catch let err {
                    print("Error adding document: \(err)")
                }
                
            case .failure(let error):
                print("Error decoding team: \(error)")
            }
        }
    }
    
    // Passes active request to RequestDisabledViewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CreateRequestSegue" {
            let nextVC = segue.destination as! RequestDisabledViewController
            let request = sender as! HelpRequest
            nextVC.helpRequest = request
            
        }
    }
}
