//
//  Project: OnTheMoov
//  Names: David Tang, Alisha Rawal, Trishton Chang, Josh Cho
//  EID: dwt572, ar85943, tbc657, jkc2796
//  Course: CS371L
//
//  SelectedTeamDetailViewController.swift
//  OnTheMoov
//
//  Created by alisha rawal on 10/31/22.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore
import FirebaseFirestoreSwift

class TeamsDisabledViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var teamLabel: UILabel!
    @IBOutlet weak var teamMembers: UITableView!
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var teamMemberLabel: UILabel!
    
    var memberList: [HelperStudent] = []
    // Firebase database
    let firestore = Firestore.firestore()
    
    // Sets text labels in view and propagates profile photo, setting dark mode if active
    override func viewWillAppear(_ animated: Bool) {
        let userDefaults = UserDefaults.standard
                if userDefaults.string(forKey: COLOR_BLIND) == ON {
                    overrideUserInterfaceStyle = .dark
                    self.view.backgroundColor = .darkGray
                } else {
                    overrideUserInterfaceStyle = .light
                    self.view.backgroundColor = UIColor(red: CGFloat(255.0/255.0), green: CGFloat(243.0/255.0), blue: CGFloat(226.0/255.0), alpha: 1)
                }
        guard let uid = Auth.auth().currentUser?.uid else { return }
        let userRef = firestore.collection("disabled").document(uid)
        userRef.getDocument(as: Team.self) { result in
         switch result {
            case .success(let team):
             self.teamLabel.text = team.team_name
            case .failure(let error):
                print("Error decoding team: \(error)")
            }
        }
        memberList = []
        getMembers()
        propogateProfilePhoto(userUID: uid)
        
        // update font size
        guard let fontSize = userDefaults.string(forKey: FONT_SIZE) else { return }
        updateLabelFontSize(fontSize: Double(fontSize)!)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        teamMembers.delegate = self
        teamMembers.dataSource = self
        // Do any additional setup after loading the view.
        
        // make profile pics circular
        profilePic.layer.masksToBounds = true
        profilePic.layer.borderWidth = 2
        profilePic.layer.borderColor = UIColor.lightGray.cgColor
        profilePic.layer.cornerRadius = (profilePic.image?.size.width)! / 2.0
    }
    
    // Updates font size of textfield
    func updateLabelFontSize(fontSize: Double) {
        teamMemberLabel.font = teamMemberLabel.font.withSize(fontSize)
    }
    
    // Add profile photo for display
    func propogateProfilePhoto(userUID: String) {
        // get the photo url of this user
        let docRef = firestore.collection("disabled").document(userUID)
        let field = "profilePhotoURL"
        
        docRef.getDocument(source: .cache) {
            (document, error) in
            if let document = document {
                // get the photo url from firestore
                let profilePhotoURL = document.get(field) as! String
                print("\n\n\n\nFetched profile photo url for this user: \(profilePhotoURL)\n\n\n\n")
                guard let url = URL(string: profilePhotoURL) else { return }
                
                // download profile photo from firestorage and display to image view
                let task = URLSession.shared.dataTask(with: url) {
                    data, _, err in
                    guard let data = data, err == nil else { return }
                    
                    DispatchQueue.main.async {
                        let image = UIImage(data: data)
                        self.profilePic.image = image
                    }
                }
                task.resume()
            } else {
                print("Document does not exist in cache")
            }
        }
    }
    
    // Retrieves team members from Firestore
    func getMembers(){
        guard let uid = Auth.auth().currentUser?.uid else { return }
        let teamRef = firestore.collection("disabled").document(uid)

        teamRef.getDocument(as: Team.self) { result in
         switch result {
            case .success(let team):
             print("Team: \(team)")
             if(!team.team.isEmpty) {
                 let teamQuery = self.firestore.collection("helper").whereField("uid", in: team.team)
                 teamQuery.getDocuments() { (querySnapshot, err) in
                     if let err = err {
                         print("Error getting documents: \(err)")
                     } else {
                         for document in querySnapshot!.documents {
                             let member = HelperStudent(snapshot: document)
                             self.memberList.append(member)
                             self.teamMembers.reloadData()
                         }
                     }
                 }
             }
             
            case .failure(let error):
                print("Error decoding team: \(error)")
            }
        }
    }
    
    
    // Count
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return memberList.count
    }
    
    // Cell selection
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = teamMembers.dequeueReusableCell(withIdentifier: "MemberCell", for: indexPath as IndexPath)
        let row = indexPath.row
        let name = memberList[row].name
        cell.textLabel?.text = name
        return cell
    }
    
    // Post selection animation
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        teamMembers.deselectRow(at: indexPath, animated: true)
    }
    
    // Allows swipe to delete users from team
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let memberUid = memberList[indexPath.row].uid
            guard let teamUid = Auth.auth().currentUser?.uid else { return }
            let teamRef = firestore.collection("disabled").document(teamUid)
            let memberRef = firestore.collection("helper").document(memberUid)
            teamRef.updateData(["team": FieldValue.arrayRemove([memberUid])])
            memberRef.updateData(["my_team": FieldValue.delete(), "active_request": FieldValue.delete()])
            
            
            
            memberList.remove(at: indexPath.row)
            teamMembers.deleteRows(at: [indexPath], with: .fade)
            
        }
    }
    
    // Passes UID of selected user to ProfileHelperViewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "MemberProfileSegue",
           let nextVC = segue.destination as? ProfileHelperViewController,
           let idx = teamMembers.indexPathForSelectedRow?.row {
            nextVC.userUID = memberList[idx].uid
        }
    }
    
}
