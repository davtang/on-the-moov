//
//  Project: OnTheMoov
//  Names: David Tang, Alisha Rawal, Trishton Chang, Josh Cho
//  EID: dwt572, ar85943, tbc657, jkc2796
//  Course: CS371L
//
//  DisabledHomeViewController.swift
//  OnTheMoov
//
//  Created by alisha rawal on 10/31/22.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore

class DisabledHomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var requestHelpButton: UIButton!
    @IBOutlet weak var recentRequests: UITableView!
    @IBOutlet weak var activeRequestButton: UIButton!
    @IBOutlet weak var recentRequestLabel: UILabel!
    
    var request_uid: String? = nil
    var requestList: [HelpRequest] = []
    
    let firestore = Firestore.firestore()
    
    // Sets text labels in view and propagates profile photo, setting dark mode if active
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let userDefaults = UserDefaults.standard
                if userDefaults.string(forKey: COLOR_BLIND) == ON {
                    overrideUserInterfaceStyle = .dark
                    self.view.backgroundColor = .darkGray
                } else {
                    overrideUserInterfaceStyle = .light
                    self.view.backgroundColor = UIColor(red: CGFloat(255.0/255.0), green: CGFloat(243.0/255.0), blue: CGFloat(226.0/255.0), alpha: 1)
                }

        guard let uid = Auth.auth().currentUser?.uid else { return }
        let userRef = firestore.collection("disabled").document(uid)
        userRef.getDocument(as: Team.self) { result in
            switch result {
            case .success(let team):
                if team.active_request != nil {
                    self.activeRequestButton.setTitle("View Active Request", for: .normal)
                    self.request_uid = team.active_request
                } else {
                    self.activeRequestButton.setTitle("No Current Active Request", for: .normal)
                }
            case .failure(let error):
                print("Error decoding team: \(error)")
            }
        }
        requestList = []
        getRecentRequests()
        
        // update font size
        guard let fontSize = userDefaults.string(forKey: FONT_SIZE) else { return }
        updateLabelFontSize(fontSize: Double(fontSize)!)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        recentRequests.delegate = self
        recentRequests.dataSource = self
        
        // Update style of request help button
        let font1 = UIFont(name: "Helvetica Neue Bold", size: 35.0)
        let font2 = UIFont(name: "Helvetica Neue", size: 30.0)
        
        // Create attributes for large and small text
        let attributesLarge: [NSAttributedString.Key: Any] = [
            .font: font1!,
            .foregroundColor: UIColor.white,
        ]
        let attributesSmall: [NSAttributedString.Key: Any] = [
            .font: font2!,
            .foregroundColor: UIColor.white,
        ]
        // Create strings
        let attributedRequest1 = NSMutableAttributedString(string: "Request help to get", attributes: attributesSmall)
        let attributedRequest2 = NSAttributedString(string: "\nOn the Moov!", attributes: attributesLarge)
        // Append Strings
        attributedRequest1.append(attributedRequest2)
        
        // Set request help button text
        requestHelpButton.setAttributedTitle(attributedRequest1, for: .normal)
        requestHelpButton.titleLabel?.textAlignment = .center
    }
    
    // Updatse font size of label
    func updateLabelFontSize(fontSize: Double) {
        recentRequestLabel.font = recentRequestLabel.font.withSize(fontSize)
    }
    
    // Returns number of requests in table
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return requestList.count
    }
    
    // Retrieves requests from Firestore
    func getRecentRequests() {
        print("GETTING RECENT REQUESTS")
        guard let uid = Auth.auth().currentUser?.uid else { return }
        let requestQuery = firestore.collection("requests").whereField("requestedUID", in: [uid])
        requestQuery.getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    print("APPENDING REQUEST")
                    let request = HelpRequest(snapshot: document)
                    if request.status == "Complete" {
                        self.requestList.append(request)
                        self.recentRequests.reloadData()
                    }
                }
            }
        }
    }
    
    // Creates cell in tableView for each request retrieved
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = recentRequests.dequeueReusableCell(withIdentifier: "RecentRequestCell", for: indexPath as IndexPath)
        let row = indexPath.row
        let meetup = requestList[row].meetupLoc
        let dest = requestList[row].dropoffLoc
        let completeTime = requestList[row].completionTime!
        cell.textLabel?.text = "\(meetup) to \(dest) at \(completeTime)"
        return cell
    }
    
    // Deselects row after selection animation
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        recentRequests.deselectRow(at: indexPath, animated: true)
    }
    
    // Segues to RequestCreationVC
    @IBAction func requestHelpPressed(_ sender: Any) {
        performSegue(withIdentifier: "CreateRequestSegue", sender: nil)
    }
    
    // Shows active request or alert if no request currently active
    @IBAction func activeRequestPressed(_ sender: Any) {
        if request_uid == nil {
            let controller = UIAlertController(
                title: "There is no current active request",
                message: "Request help to get On the Moov!",
                preferredStyle: .alert)
            
            controller.addAction(UIAlertAction(title: "OK", style: .default))
            present(controller, animated: true)
        } else {
            let helpReqRef = firestore.collection("requests").document(request_uid!)
            helpReqRef.getDocument(as: HelpRequest.self) { result in
                switch result {
                case .success(let request):
                    if request.status == "Pending" {
                        self.performSegue(withIdentifier: "DetailedRequestSegue", sender: request)
                    } else if request.status == "Accepted" {
                        self.performSegue(withIdentifier: "AcceptedRequestSegue", sender: request)
                    } else {
                        print("Error illegal status of request: \(request.status)")
                    }

                case .failure(let error):
                    print("Error decoding team: \(error)")
                }
            }
        }
    }
    
    // Passes help request to appropriate VC
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DetailedRequestSegue" {
            let nextVC = segue.destination as! RequestDisabledViewController
            let request = sender as! HelpRequest
            nextVC.helpRequest = request
        } else if segue.identifier == "AcceptedRequestSegue" {
            let nextVC = segue.destination as! RequestAcceptedDisabledViewController
            let request = sender as! HelpRequest
            nextVC.helpRequest = request
        } else if segue.identifier == "RecentRequestSegue",
              let nextVC = segue.destination as? RequestCompleteDisabledViewController,
              let requestIdx = recentRequests.indexPathForSelectedRow?.row {
              let selectedReq = requestList[requestIdx]
              nextVC.helpRequest = selectedReq
          }
    }
}
