//
//  Project: OnTheMoov
//  Names: David Tang, Alisha Rawal, Trishton Chang, Josh Cho
//  EID: dwt572, ar85943, tbc657, jkc2796
//  Course: CS371L
//
//  ProfileCreationViewController.swift
//  OnTheMoov
//
//  Created by David Tang on 10/13/22.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore
import FirebaseFirestoreSwift
import FirebaseMessaging
import FirebaseStorage


class ProfileCreationViewController: UIViewController, UITextFieldDelegate {

    var isDisabledStudent = false
    var studentYear = "First"
    
    @IBOutlet weak var associationLabel: UILabel!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var associationField: UITextField!
    @IBOutlet weak var pronounsField: UITextField!
    @IBOutlet weak var yearCtrl: UISegmentedControl!
    @IBOutlet weak var bioField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var errorMessage: UILabel!
    @IBOutlet weak var profilePhoto: UIImageView!
    
    // Shows dark mode if active
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let userDefaults = UserDefaults.standard
                if userDefaults.string(forKey: COLOR_BLIND) == ON {
                    overrideUserInterfaceStyle = .dark
                    self.view.backgroundColor = .darkGray
                } else {
                    overrideUserInterfaceStyle = .light
                    self.view.backgroundColor = UIColor(red: CGFloat(255.0/255.0), green: CGFloat(243.0/255.0), blue: CGFloat(226.0/255.0), alpha: 1)
                }
        if(isDisabledStudent) {
            associationLabel.text = "Team Name:"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // remove keyboard when tapping
        nameField.delegate = self
        associationField.delegate = self
        pronounsField.delegate = self
        bioField.delegate = self
        emailField.delegate = self
        passwordField.delegate = self

        Auth.auth().addStateDidChangeListener() {
            auth, user in
            if user != nil {
                if(self.isDisabledStudent) {
                    self.performSegue(withIdentifier: "RegistrationDisabledSegue", sender: nil)
                } else {
                    self.performSegue(withIdentifier: "RegistrationLoginSegue", sender: nil)
                }
                
            }
        }
        
        // Make profile pics circular
        profilePhoto.layer.masksToBounds = true
        profilePhoto.layer.borderWidth = 2
        profilePhoto.layer.borderColor = UIColor.lightGray.cgColor
        profilePhoto.layer.cornerRadius = (profilePhoto.image?.size.width)! / 2.0
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // Dismisses keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // Changes studentYear to reflect selected segment
    @IBAction func onYearSegmentChanged(_ sender: Any) {
        switch yearCtrl.selectedSegmentIndex {
        case 0:
            studentYear = "First"
        case 1:
            studentYear = "Second"
        case 2:
            studentYear = "Third"
        case 3:
            studentYear = "Fourth"
        case 4:
            studentYear = "Other"
        default:
            studentYear = "Invalid"
        }
    }

    // Creates user profile with provided credentials
    @IBAction func createProfilePressed(_ sender: Any) {
        guard let email = emailField.text,
              let password = passwordField.text,
              !email.isEmpty && !password.isEmpty else {
            errorMessage.text = "Please enter your email and password."
            return
        }
        
        
        // Create auth user now
        Auth.auth().createUser(withEmail: emailField.text!, password: passwordField.text!) {
            authResult, error in
            if let error = error as NSError? {
                self.errorMessage.text = "\(error.localizedDescription)"
            } else {
                self.errorMessage.text = ""
            }
            // upload profile photo to storage and upload user info
            self.persistImageToStorage()
        }
    }
    
    // Saves user info into Firestore
    func storeUserInformation(profilePhotoURL:String){
        guard let uid = Auth.auth().currentUser?.uid else { return }
        let firestore = Firestore.firestore()
        let fcmtoken = Messaging.messaging().fcmToken
        if (isDisabledStudent){
            let userData = Team(teamName: self.associationField.text!, name: self.nameField.text!, pronouns: self.pronounsField.text!, year: self.studentYear, bio: self.bioField.text!, email: self.emailField.text!, uid: uid, fcmToken: fcmtoken!, profilePhotoURL: profilePhotoURL)
            
            do {
                try firestore.collection("disabled").document(uid).setData(from:userData)
            } catch let error {
                print("\n\n\n\nError writing user data to Firestore: \(error)\n\n\n\n")
            }
        } else {
            let userData = HelperStudent(name: self.nameField.text!, org: self.associationField.text!, pronouns: self.pronounsField.text!, year: self.studentYear, bio: self.bioField.text!, email: self.emailField.text!, uid: uid, fcmToken: fcmtoken!, profilePhotoURL: profilePhotoURL)
            
            do {
                try firestore.collection("helper").document(uid).setData(from:userData)
            } catch let error {
                print("\n\n\nError writing user data to Firestore: \(error)\n\n\n\n")
            }
        }
    }
    
    @IBAction func changeProfilePhotoButtonPressed(_ sender: Any) {
        presentPhotoActionSheet()
    }
}



// Extension for profile photo capability
extension ProfileCreationViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func presentPhotoActionSheet() {
        let actionSheet = UIAlertController(title: "Profile Picture",
                                            message: "How would you like to select a picture?",
                                            preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Cancel",
                                            style: .cancel))
        actionSheet.addAction(UIAlertAction(title: "Take Photo",
                                            style: .default) {
            [weak self] _ in
            self?.presentCamera()
        })
        actionSheet.addAction(UIAlertAction(title: "Choose From Gallery",
                                            style: .default) {
            [weak self] _ in
            self?.presentPhotoPicker()
        })
        
        // show to user now
        present(actionSheet, animated: true)
    }
    
    // let user take camera photo for profile
    func presentCamera() {
        let vc = UIImagePickerController()
        vc.sourceType = .camera
        vc.delegate = self
        vc.allowsEditing = true  // let user crop the photo
        present(vc, animated: true)
    }
    
    // let user choose photo for profile
    func presentPhotoPicker() {
        let vc = UIImagePickerController()
        vc.sourceType = .photoLibrary
        vc.delegate = self
        vc.allowsEditing = true  // let user crop the photo
        present(vc, animated: true)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        guard let selectedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else {
            return
        }
        self.profilePhoto.image = selectedImage
    }
    
    // store image into firebase storage
    func persistImageToStorage() {
        guard let uid = Auth.auth().currentUser?.uid else {
            return
        }
        let ref = Storage.storage().reference(withPath: uid)
        
        // make the profile photo into a jpeg
        guard let imageData = self.profilePhoto.image?.jpegData(compressionQuality: 0.5) else {
            return
        }
        
        ref.putData(imageData) {
            metadata, err in
            if let err = err {
                print("\n\n\n\n\nError in putting profile photo into firebase storage, \(err)\n\n\n\n")
                return
            }
            ref.downloadURL {
                url, err in
                if let err = err {
                    print("\n\n\n\nError in retrieving download URL, \(err)\n\n\n\n")
                    return
                }
                print("\n\n\n\nSuccesfully stored photo into firebase storage with url: \(url?.absoluteString ?? "")\n\n\n\n")
                
                // now store to firestore userdata
                let profilePhotoURL:String = url?.absoluteString ?? ""
                self.storeUserInformation(profilePhotoURL: profilePhotoURL)
            }
        }
    }
}

