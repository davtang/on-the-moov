//
//  Project: OnTheMoov
//  Names: David Tang, Alisha Rawal, Trishton Chang, Josh Cho
//  EID: dwt572, ar85943, tbc657, jkc2796
//  Course: CS371L
//
//  RecentRequestDetailedViewController.swift
//  OnTheMoov
//
//  Created by alisha rawal on 10/31/22.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore

class RecentRequestDetailedViewController: UIViewController {

    @IBOutlet weak var fromLabel: UILabel!
    @IBOutlet weak var toLabel: UILabel!
    @IBOutlet weak var disabledPronouns: UILabel!
    @IBOutlet weak var requestType: UILabel!
    @IBOutlet weak var studentName: UILabel!
    @IBOutlet weak var picture: UIImageView!
    @IBOutlet weak var requestedTimeLabel: UILabel!
    @IBOutlet weak var dropoffTimeLabel: UILabel!
    
    var helpRequest:HelpRequest? = nil
    // Firebase database
    let database = Firestore.firestore()
    
    // Sets text labels in view and propagates profile photo, setting dark mode if active
    override func viewWillAppear(_ animated: Bool) {
        guard let helpReq = helpRequest else {
            return
        }
        let userDefaults = UserDefaults.standard
                if userDefaults.string(forKey: COLOR_BLIND) == ON {
                    overrideUserInterfaceStyle = .dark
                    self.view.backgroundColor = .darkGray
                } else {
                    overrideUserInterfaceStyle = .light
                    self.view.backgroundColor = UIColor(red: CGFloat(255.0/255.0), green: CGFloat(243.0/255.0), blue: CGFloat(226.0/255.0), alpha: 1)
                }

        
        fromLabel.text = helpReq.meetupLoc
        toLabel.text = helpReq.dropoffLoc
        disabledPronouns.text = helpReq.reqPronouns
        requestedTimeLabel.text = helpReq.requestedTime
        studentName.text = helpReq.requestedBy
        requestType.text = helpReq.requestType
        dropoffTimeLabel.text = helpReq.dropoffTime
        
        propogateProfilePhoto(helpReqUid: helpReq.requestedUID)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // make profile pics circular
        picture.layer.masksToBounds = true
        picture.layer.borderWidth = 2
        picture.layer.borderColor = UIColor.lightGray.cgColor
        picture.layer.cornerRadius = (picture.image?.size.width)! / 2.0
    }
    
    // Add profile photo for display
    func propogateProfilePhoto(helpReqUid: String) {
        // Get the photo url of this user
        let docRef = database.collection("disabled").document(helpReqUid)
        let field = "profilePhotoURL"
        
        docRef.getDocument(source: .cache) {
            (document, error) in
            if let document = document {
                // Get the photo url from firestore
                let profilePhotoURL = document.get(field) as! String
                print("\n\n\n\nFetched profile photo url for this user: \(profilePhotoURL)\n\n\n\n")
                guard let url = URL(string: profilePhotoURL) else { return }
                
                // Download profile photo from firestorage and display to image view
                let task = URLSession.shared.dataTask(with: url) {
                    data, _, err in
                    guard let data = data, err == nil else { return }
                    
                    DispatchQueue.main.async {
                        let image = UIImage(data: data)
                        self.picture.image = image
                    }
                }
                task.resume()
            } else {
                print("Document does not exist in cache")
            }
        }
    }
    
    // Segue to home screen if request rejected
    @IBAction func noButton(_ sender: Any) {
    }
    
    // Accepts request, removing from all other users' active and updating Firestore
    @IBAction func yesButton(_ sender: Any) {
        guard let helpReq = helpRequest else {
            return
        }
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        let firestore = Firestore.firestore()
        
        let userRef = firestore.collection("helper").document(uid)
        userRef.getDocument(as: HelperStudent.self) { result in
            switch result {
            case .success(let helper):
                let reqRef = firestore.collection("requests").document(helpReq.uid!)
                reqRef.updateData(["status": "Accepted", "acceptedBy": helper.name, "accPronouns": helper.pronouns, "acceptedUID": uid])
                let teamRef = firestore.collection("disabled").document(helper.my_team!)
                teamRef.getDocument(as: Team.self) { result in
                    switch result {
                    case .success(let team):
                        for member in team.team {
                            if member != uid {
                                let memberRef = firestore.collection("helper").document(member)
                                memberRef.updateData(["active_request": FieldValue.delete()])
                            }
                        }
                    case .failure(let error):
                        print("Error decoding user: \(error)")
                    }
                }
            case .failure(let error):
                print("Error decoding user: \(error)")
            }
        }
        performSegue(withIdentifier: "AcceptRequestSegue", sender: helpReq)
    }
    
    // Passes active request to ActiveRequestDetailViewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AcceptRequestSegue" {
            let nextVC = segue.destination as! ActiveRequestDetailViewController
            let request = sender as! HelpRequest
            nextVC.helpRequest = request
        }
    }

}
