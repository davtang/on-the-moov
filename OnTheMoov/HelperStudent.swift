//
//  Project: OnTheMoov
//  Names: David Tang, Alisha Rawal, Trishton Chang, Josh Cho
//  EID: dwt572, ar85943, tbc657, jkc2796
//  Course: CS371L
//
//  HelperStudent.swift
//  OnTheMoov
//
//  Created by David Tang on 10/18/22.
//

import FirebaseFirestore

// Represents a helper student, aligns with Collection on Firestore
struct HelperStudent: Codable {
    var name: String
    var my_team: String?
    var organization: String
    var pronouns: String
    var year: String
    var bio: String
    var email: String
    var uid: String
    var active_request: String?
    var completed_requests: [String]?
    var fcm_token: String
    var profilePhotoURL: String
    
    init(name: String, org: String, pronouns: String, year: String, bio: String, email:String, uid: String, fcmToken: String, profilePhotoURL: String) {
        self.name = name
        self.organization = org
        self.pronouns = pronouns
        self.year = year
        self.bio = bio
        self.email = email
        self.uid = uid
        self.active_request = nil
        self.completed_requests = nil
        self.fcm_token = fcmToken
        self.profilePhotoURL = profilePhotoURL
        self.my_team = nil
    }
    
    init(snapshot: DocumentSnapshot) {
        let snapshotValue = snapshot.data()!
        self.name = snapshotValue["name"] as! String
        self.organization = snapshotValue["organization"] as! String
        self.pronouns = snapshotValue["pronouns"] as! String
        self.year = snapshotValue["year"] as! String
        self.bio = snapshotValue["bio"] as! String
        self.email = snapshotValue["email"] as! String
        self.uid = snapshotValue["uid"] as! String
        self.fcm_token = snapshotValue["fcm_token"] as! String
        self.active_request = snapshotValue["active_request"] as? String
        self.completed_requests = snapshotValue["completed_requests"] as? [String]
        self.profilePhotoURL = snapshotValue["profilePhotoURL"] as! String
        self.my_team = snapshotValue["my_team"] as? String
    }

}
