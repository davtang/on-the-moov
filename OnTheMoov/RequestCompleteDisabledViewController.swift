//
//  Project: OnTheMoov
//  Names: David Tang, Alisha Rawal, Trishton Chang, Josh Cho
//  EID: dwt572, ar85943, tbc657, jkc2796
//  Course: CS371L
//
//  RequestCompleteDisabledViewController.swift
//  OnTheMoov
//
//  Created by alisha rawal on 10/31/22.
//

import UIKit
import FirebaseFirestore

class RequestCompleteDisabledViewController: UIViewController {

    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var completedTimeLabel: UILabel!
    @IBOutlet weak var helperNameLabel: UILabel!
    @IBOutlet weak var teamNameLabel: UILabel!
    @IBOutlet weak var requestTypeLabel: UILabel!
    @IBOutlet weak var meetupLocLabel: UILabel!
    @IBOutlet weak var destLocLabel: UILabel!
    @IBOutlet weak var profilePic: UIImageView!
    
    var helpRequest:HelpRequest? = nil
    // Firebase database
    let database = Firestore.firestore()
    
    // Sets text labels in view and propagates profile photo, setting dark mode if active
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let helpReq = helpRequest else {
            return
        }
        let userDefaults = UserDefaults.standard
                if userDefaults.string(forKey: COLOR_BLIND) == ON {
                    overrideUserInterfaceStyle = .dark
                    self.view.backgroundColor = .darkGray
                } else {
                    overrideUserInterfaceStyle = .light
                    self.view.backgroundColor = UIColor(red: CGFloat(255.0/255.0), green: CGFloat(243.0/255.0), blue: CGFloat(226.0/255.0), alpha: 1)
                }
        
        completedTimeLabel.text = helpReq.completionTime
        helperNameLabel.text = helpReq.acceptedBy
        teamNameLabel.text = helpReq.teamName
        requestTypeLabel.text = helpReq.requestType
        meetupLocLabel.text = helpReq.meetupLoc
        destLocLabel.text = helpReq.dropoffLoc
        statusLabel.text = "Status: " + helpReq.status
        
        propogateProfilePhoto(helpReqUid: helpReq.acceptedUID!)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Make profile pics circular
        profilePic.layer.masksToBounds = true
        profilePic.layer.borderWidth = 2
        profilePic.layer.borderColor = UIColor.lightGray.cgColor
        profilePic.layer.cornerRadius = (profilePic.image?.size.width)! / 2.0
    }

    // Add profile photo for display
    func propogateProfilePhoto(helpReqUid: String) {
        // Get the photo url of this user
        let docRef = database.collection("helper").document(helpReqUid)
        let field = "profilePhotoURL"
        
        docRef.getDocument(source: .cache) {
            (document, error) in
            if let document = document {
                // Get the photo url from firestore
                let profilePhotoURL = document.get(field) as! String
                print("\n\n\n\nFetched profile photo url for this user: \(profilePhotoURL)\n\n\n\n")
                guard let url = URL(string: profilePhotoURL) else { return }
                
                // Download profile photo from firestorage and display to image view
                let task = URLSession.shared.dataTask(with: url) {
                    data, _, err in
                    guard let data = data, err == nil else { return }
                    
                    DispatchQueue.main.async {
                        let image = UIImage(data: data)
                        self.profilePic.image = image
                    }
                }
                task.resume()
            } else {
                print("Document does not exist in cache")
            }
        }
    }
}
