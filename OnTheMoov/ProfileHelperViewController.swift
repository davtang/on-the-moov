//
//  Project: OnTheMoov
//  Names: David Tang, Alisha Rawal, Trishton Chang, Josh Cho
//  EID: dwt572, ar85943, tbc657, jkc2796
//  Course: CS371L
//
//  ProfileHelperViewController.swift
//  OnTheMoov
//
//  Created by alisha rawal on 10/31/22.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore
import FirebaseFirestoreSwift

class ProfileHelperViewController: UIViewController {

    
    @IBOutlet weak var nameField: UILabel!
    @IBOutlet weak var pronounsField: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var orgLabel: UILabel!
    @IBOutlet weak var bioLabel: UILabel!
    @IBOutlet weak var profilePic: UIImageView!
    
    // Firebase database
    let database = Firestore.firestore()
    var userUID:String = ""
    
    // Sets text labels in view and propagates profile photo, setting dark mode if active
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let userDefaults = UserDefaults.standard
                if userDefaults.string(forKey: COLOR_BLIND) == ON {
                    overrideUserInterfaceStyle = .dark
                    self.view.backgroundColor = .darkGray
                } else {
                    overrideUserInterfaceStyle = .light
                    self.view.backgroundColor = UIColor(red: CGFloat(255.0/255.0), green: CGFloat(243.0/255.0), blue: CGFloat(226.0/255.0), alpha: 1)
                }
        propogateProfilePhoto()
        
        // update font size
        guard let fontSize = userDefaults.string(forKey: FONT_SIZE) else { return }
        updateLabelFontSize(fontSize: Double(fontSize)!)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getUserInfo()
        // Do any additional setup after loading the view.
        
        // make profile pics circular
        profilePic.layer.masksToBounds = true
        profilePic.layer.borderWidth = 2
        profilePic.layer.borderColor = UIColor.lightGray.cgColor
        profilePic.layer.cornerRadius = (profilePic.image?.size.width)! / 2.0
    }
    
    // Update font size of labels
    func updateLabelFontSize(fontSize: Double) {
        nameField.font = nameField.font.withSize(fontSize)
        pronounsField.font = pronounsField.font.withSize(fontSize)
        yearLabel.font = yearLabel.font.withSize(fontSize)
        bioLabel.font = bioLabel.font.withSize(fontSize)
        orgLabel.font = orgLabel.font.withSize(fontSize)
    }
    
    // Retrieves user info from Firestore
    func getUserInfo(){
        let firestore = Firestore.firestore()
        let userRef = firestore.collection("helper").document(userUID)
        userRef.getDocument(as: HelperStudent.self) { result in
         switch result {
            case .success(let user):
                self.nameField.text = user.name
                self.pronounsField.text = user.pronouns
                self.yearLabel.text = user.year
                self.orgLabel.text = user.organization
                self.bioLabel.text = user.bio
            case .failure(let error):
                print("Error decoding user: \(error)")
            }
        }
    }
    
    // Add profile photo for display
    func propogateProfilePhoto() {
        // get the photo url of this user
        let docRef = database.collection("helper").document(userUID)
        let field = "profilePhotoURL"
        
        docRef.getDocument(source: .cache) {
            (document, error) in
            if let document = document {
                // get the photo url from firestore
                let profilePhotoURL = document.get(field) as! String
                print("\n\n\n\nFetched profile photo url for this user: \(profilePhotoURL)\n\n\n\n")
                guard let url = URL(string: profilePhotoURL) else { return }
                
                // download profile photo from firestorage and display to image view
                let task = URLSession.shared.dataTask(with: url) {
                    data, _, err in
                    guard let data = data, err == nil else { return }
                    
                    DispatchQueue.main.async {
                        let image = UIImage(data: data)
                        self.profilePic.image = image
                    }
                }
                task.resume()
            } else {
                print("Document does not exist in cache")
            }
        }
    }
}
