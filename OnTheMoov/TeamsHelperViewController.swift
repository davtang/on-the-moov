//
//  Project: OnTheMoov
//  Names: David Tang, Alisha Rawal, Trishton Chang, Josh Cho
//  EID: dwt572, ar85943, tbc657, jkc2796
//  Course: CS371L
//
//  TeamsHelperViewController.swift
//  OnTheMoov
//
//  Created by alisha rawal on 10/31/22.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore
import FirebaseFirestoreSwift


class TeamsHelperViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var availableTeams: UITableView!
    @IBOutlet weak var myTeam: UIButton!
    @IBOutlet weak var myTeamLabel: UILabel!
    @IBOutlet weak var availableTeamsLabel: UILabel!
    
    let textCellIdentifier2 = "TextCell2"
    var myTeamId = ""
    var teamList: [Team] = []
    
    // Sets dark mode if active and font size
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let userDefaults = UserDefaults.standard
                if userDefaults.string(forKey: COLOR_BLIND) == ON {
                    overrideUserInterfaceStyle = .dark
                    self.view.backgroundColor = .darkGray
                } else {
                    overrideUserInterfaceStyle = .light
                    self.view.backgroundColor = UIColor(red: CGFloat(255.0/255.0), green: CGFloat(243.0/255.0), blue: CGFloat(226.0/255.0), alpha: 1)
                }
        getMyTeam()
        
        // update font size
        guard let fontSize = userDefaults.string(forKey: FONT_SIZE) else { return }
        updateLabelFontSize(fontSize: Double(fontSize)!)
    }
    
    // Retrieves team from Firestore
    override func viewDidLoad() {
        super.viewDidLoad()
        availableTeams.delegate = self
        availableTeams.dataSource = self
        self.myTeam.setTitle("No team yet, join a team below", for: .normal)
        teamList = []
        getAllTeams()
    }
    
    // Updates text field font size
    func updateLabelFontSize(fontSize: Double) {
        myTeamLabel.font = myTeamLabel.font.withSize(fontSize)
        availableTeamsLabel.font = availableTeamsLabel.font.withSize(fontSize)
    }
    
    // Segues to team detailed view or shows alert if no team joined
    @IBAction func teamButton(_ sender: Any) {
        if myTeam.titleLabel?.text == "No team yet, join a team below" {
            let controller = UIAlertController(
                title: "You are not in a team",
                message: "Browse the teams below to join one",
                preferredStyle: .alert)
            
            controller.addAction(UIAlertAction(title: "OK", style: .default))
            present(controller, animated: true)
        } else {
            performSegue(withIdentifier: "MyTeamSegue", sender: nil)
        }
    }
    
    // Retrieves users part of this team, sets button title accordingly
    func getMyTeam(){
        guard let uid = Auth.auth().currentUser?.uid else { return }
        let firestore = Firestore.firestore()
        let userRef = firestore.collection("helper").document(uid)
        // Get my team
        userRef.getDocument(as: HelperStudent.self) { result in
             switch result {
                case .success(let helper):
                 if let teamUID = helper.my_team, teamUID != "" {
                     self.myTeam.setTitle("View My Team", for: .normal)
                     self.myTeamId = helper.my_team!
                 } else {
                     self.myTeam.setTitle("No team yet, join a team below", for: .normal)
                 }
                case .failure(let error):
                    print("Error decoding team: \(error)")
            }
        }
    }
    
    // Retrieves all available teams from Firestore
    func getAllTeams(){
        let firestore = Firestore.firestore()
        // Get all available teams
        firestore.collection("disabled").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    let data = document.data()
                    let uid = data["uid"] as? String ?? ""
                    let teamRef = firestore.collection("disabled").document(uid)
                    teamRef.getDocument(as: Team.self) { result in
                         switch result {
                            case .success(let team):
                                self.teamList.append(team)
                                self.availableTeams.reloadData()
                            case .failure(let error):
                                print("Error decoding team: \(error)")
                        }
                    }
                 }
            }
        }
    }
    
    // Count
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return teamList.count
    }
    
    // Cell selection
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = availableTeams.dequeueReusableCell(withIdentifier: textCellIdentifier2, for: indexPath as IndexPath)
        let row = indexPath.row
        let teamName = teamList[row].team_name
        cell.textLabel?.text = teamName
        return cell
    }
    
    // Post selection animation
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        availableTeams.deselectRow(at: indexPath, animated: true)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "TeamDetailedSegue",
            let nextVC = segue.destination as? SelectedTeamDetailViewController,
            let idx = availableTeams.indexPathForSelectedRow?.row {
            nextVC.teamUID = teamList[idx].uid
        } else if segue.identifier == "MyTeamSegue",
                  let nextVC = segue.destination as? SelectedTeamDetailViewController{
            nextVC.teamUID = myTeamId
        }
    }
}
