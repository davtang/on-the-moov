//
//  Project: OnTheMoov
//  Names: David Tang, Alisha Rawal, Trishton Chang, Josh Cho
//  EID: dwt572, ar85943, tbc657, jkc2796
//  Course: CS371L
//
//  RequestDisabledViewController.swift
//  OnTheMoov
//
//  Created by alisha rawal on 10/31/22.
//

import UIKit
import FirebaseFirestore

class RequestDisabledViewController: UIViewController {

    @IBOutlet weak var teamName: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var submitTimeLabel: UILabel!
    @IBOutlet weak var destLoc: UILabel!
    @IBOutlet weak var meetupLoc: UILabel!
    @IBOutlet weak var pictureLoad: UIActivityIndicatorView!
    @IBOutlet weak var statusLabel: UILabel!
    

    var helpRequest: HelpRequest? = nil
    
    // Sets text labels in view, setting dark mode if active
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let helpReq = helpRequest else {
            return
        }
        print(helpReq.status)
        teamName.text = helpReq.teamName
        timeLabel.text = helpReq.dropoffTime
        submitTimeLabel.text = helpReq.requestedTime
        destLoc.text = helpReq.dropoffLoc
        meetupLoc.text = helpReq.meetupLoc
        statusLabel.text = "Status: " + helpReq.status
        let userDefaults = UserDefaults.standard
                if userDefaults.string(forKey: COLOR_BLIND) == ON {
                    overrideUserInterfaceStyle = .dark
                    self.view.backgroundColor = .darkGray
                } else {
                    overrideUserInterfaceStyle = .light
                    self.view.backgroundColor = UIColor(red: CGFloat(255.0/255.0), green: CGFloat(243.0/255.0), blue: CGFloat(226.0/255.0), alpha: 1)
                }

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // Segues if request status has changed
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        guard let helpReq = helpRequest else {
            return
        }
        if helpReq.status != "Pending" {
            self.performSegue(withIdentifier: "AcceptedRequestSegue", sender: helpReq)
        } else {
            let firestore = Firestore.firestore()
            firestore.collection("requests").document(helpReq.uid!).addSnapshotListener {
                documentSnapshot, error in
                guard let document = documentSnapshot else {
                    print("Error fetching document: \(error!)")
                    return
                }
                if document.exists {
                    let helpRequestDoc = HelpRequest(snapshot: document)
                    if helpRequestDoc.status != "Pending" {
                        self.performSegue(withIdentifier: "AcceptedRequestSegue", sender: helpRequestDoc)
                    }
                }
            }
        }
    }
    
    // Passes current request to RequestAcceptedDisabledViewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AcceptedRequestSegue" {
            let nextVC = segue.destination as! RequestAcceptedDisabledViewController
            let request = sender as! HelpRequest
            nextVC.helpRequest = request
        }
    }

}
