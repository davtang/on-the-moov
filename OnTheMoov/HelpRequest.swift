//
//  Project: OnTheMoov
//  Names: David Tang, Alisha Rawal, Trishton Chang, Josh Cho
//  EID: dwt572, ar85943, tbc657, jkc2796
//  Course: CS371L
//
//  HelpRequest.swift
//  OnTheMoov
//
//  Created by David Tang on 11/3/22.
//

import FirebaseFirestore

// Represents a Help Request, aligns with Collection on Firestore
class HelpRequest: Codable {
    var requestType: String
    var dropoffTime: String
    var requestedTime: String
    var dropoffLoc: String
    var meetupLoc: String
    var teamName: String
    var requestedBy: String
    var requestedUID: String
    var reqPronouns: String
    var acceptedBy: String?
    var accPronouns: String?
    var acceptedUID: String?
    var status: String
    var completionTime: String?
    var uid: String?
    
    init(reqType: String, doTime: String, reqTime: String, doLoc: String, muLoc: String, requester: String, requesterUID:String, pronouns: String, team: String) {
        self.requestType = reqType
        self.dropoffTime = doTime
        self.requestedTime = reqTime
        self.dropoffLoc = doLoc
        self.meetupLoc = muLoc
        self.teamName = team
        self.requestedBy = requester
        self.requestedUID = requesterUID
        self.reqPronouns = pronouns
        self.status = "Pending"
    }
    
    init(snapshot: DocumentSnapshot) {
        let snapshotValue = snapshot.data()!
        self.requestType = snapshotValue["requestType"] as! String
        self.dropoffTime = snapshotValue["dropoffTime"] as! String
        self.requestedTime = snapshotValue["requestedTime"] as! String
        self.dropoffLoc = snapshotValue["dropoffLoc"] as! String
        self.meetupLoc = snapshotValue["meetupLoc"] as! String
        self.teamName = snapshotValue["teamName"] as! String
        self.requestedBy = snapshotValue["requestedBy"] as! String
        self.requestedUID = snapshotValue["requestedUID"] as! String
        self.reqPronouns = snapshotValue["reqPronouns"] as! String
        self.status = snapshotValue["status"] as! String
        self.uid = snapshotValue["uid"] as? String
        self.acceptedBy = snapshotValue["acceptedBy"] as? String
        self.accPronouns =
            snapshotValue["accPronouns"] as? String
        self.acceptedUID = snapshotValue["acceptedUID"] as? String
        self.completionTime = snapshotValue["completionTime"] as? String
    }
    
    func setUID(uid: String) {
        self.uid = uid
    }
}
