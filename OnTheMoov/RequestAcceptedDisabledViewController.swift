//
//  Project: OnTheMoov
//  Names: David Tang, Alisha Rawal, Trishton Chang, Josh Cho
//  EID: dwt572, ar85943, tbc657, jkc2796
//  Course: CS371L
//
//  RequestAcceptedDisabledViewController.swift
//  OnTheMoov
//
//  Created by David Tang on 11/24/22.
//

import UIKit
import FirebaseFirestore

class RequestAcceptedDisabledViewController: UIViewController {

    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var helperNameLabel: UILabel!
    @IBOutlet weak var helperPronounsLabel: UILabel!
    @IBOutlet weak var teamNameLabel: UILabel!
    @IBOutlet weak var requestTypeLabel: UILabel!
    @IBOutlet weak var meetupLocLabel: UILabel!
    @IBOutlet weak var destLocLabel: UILabel!
    @IBOutlet weak var profilePic: UIImageView!
    
    var helpRequest:HelpRequest? = nil
    // Firebase database
    let database = Firestore.firestore()
    
    // Sets text labels in view and propagates profile photo, setting dark mode if active
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let helpReq = helpRequest else {
            return
        }
        let userDefaults = UserDefaults.standard
                if userDefaults.string(forKey: COLOR_BLIND) == ON {
                    overrideUserInterfaceStyle = .dark
                    self.view.backgroundColor = .darkGray
                } else {
                    overrideUserInterfaceStyle = .light
                    self.view.backgroundColor = UIColor(red: CGFloat(255.0/255.0), green: CGFloat(243.0/255.0), blue: CGFloat(226.0/255.0), alpha: 1)
                }

        
        helperNameLabel.text = helpReq.acceptedBy
        helperPronounsLabel.text = helpReq.accPronouns
        teamNameLabel.text = helpReq.teamName
        requestTypeLabel.text = helpReq.requestType
        meetupLocLabel.text = helpReq.meetupLoc
        destLocLabel.text = helpReq.dropoffLoc
        statusLabel.text = "Status: " + helpReq.status
        
        propogateProfilePhoto(helpReqUid: helpReq.acceptedUID!)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Make profile pics circular
        profilePic.layer.masksToBounds = true
        profilePic.layer.borderWidth = 2
        profilePic.layer.borderColor = UIColor.lightGray.cgColor
        profilePic.layer.cornerRadius = (profilePic.image?.size.width)! / 2.0
    }
    
    // Segues if request status has changed
    override func viewDidAppear(_ animated: Bool) {
        guard let helpReq = helpRequest else {
            return
        }
        if helpReq.status == "Completed" {
            self.performSegue(withIdentifier: "CompletedRequestSegue", sender: helpReq)
        } else {
            let firestore = Firestore.firestore()
            firestore.collection("requests").document(helpReq.uid!).addSnapshotListener {
                documentSnapshot, error in
                guard let document = documentSnapshot else {
                    print("Error fetching document: \(error!)")
                    return
                }
                if document.exists {
                    let helpRequestDoc = HelpRequest(snapshot: document)
                    if helpRequestDoc.status == "Completed" {
                        self.performSegue(withIdentifier: "CompletedRequestSegue", sender: helpRequestDoc)
                    }
                }
            }
        }
    }
    
    // Passes current request to RequestCompleteDisabledViewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CompletedRequestSegue" {
            let nextVC = segue.destination as! RequestCompleteDisabledViewController
            let request = sender as! HelpRequest
            nextVC.helpRequest = request
        }
    }
    
    // Add profile photo for display
    func propogateProfilePhoto(helpReqUid: String) {
        // Get the photo url of this user
        let docRef = database.collection("helper").document(helpReqUid)
        let field = "profilePhotoURL"
        
        docRef.getDocument(source: .cache) {
            (document, error) in
            if let document = document {
                // Get the photo url from firestore
                let profilePhotoURL = document.get(field) as! String
                print("\n\n\n\nFetched profile photo url for this user: \(profilePhotoURL)\n\n\n\n")
                guard let url = URL(string: profilePhotoURL) else { return }
                
                // Download profile photo from firestorage and display to image view
                let task = URLSession.shared.dataTask(with: url) {
                    data, _, err in
                    guard let data = data, err == nil else { return }
                    
                    DispatchQueue.main.async {
                        let image = UIImage(data: data)
                        self.profilePic.image = image
                    }
                }
                task.resume()
            } else {
                print("Document does not exist in cache")
            }
        }
    }
}
